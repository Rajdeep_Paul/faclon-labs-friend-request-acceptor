const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const userRouter = require('./routes/userRouter')
dotenv.config()
const app = express();
app.use(express.json())

app.use('/', userRouter)


const URI = process.env.MONGODB_URI
const PORT = process.env.PORT || 5000
mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`server running on port ${PORT}`)))
    .catch((e) => console.log(e))
