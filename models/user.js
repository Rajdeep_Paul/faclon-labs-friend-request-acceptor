const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    friends: [mongoose.Schema.Types.ObjectId],
    pending: [mongoose.Schema.Types.ObjectId],
    sentRequest: [mongoose.Schema.Types.ObjectId]
})
module.exports = mongoose.model("User", userSchema)