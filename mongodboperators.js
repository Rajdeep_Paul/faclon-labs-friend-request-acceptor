const User = require('./models/user')

//equal operator
User.find({ "name": { $eq: "abc" } })

// not equal is $ne 
User.find({ "name": { $ne: "abc" } })

//in operator return all documents which contains any one of the values passed in the in array
User.find({ "friends": { $in: [id1, id2, id3] } })

//or operators return all document which satisfy any of the condition
User.find({ $or: [{ "name": "a" }, { "email": "z@gmail.com" }] })

//pull operators removes a value from the array
User.updateMany({}, { $pull: { friends: myid } })

//push operators adds a value into the array
User.updateMany({}, { $push: { friends: myid } })
