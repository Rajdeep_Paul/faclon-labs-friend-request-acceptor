### equal operator eq

```User.find({ "name": { $eq: "abc" } })```

### not equal ne

```User.find({ "name": { $ne: "abc" } })```

### gt =greater than
### lt  =less than
### gte =greater than or equal
### lte  =less than or equal


### in operator return all documents which contains any one of the values passed in the in array

```User.find({ "friends": { $in: [id1, id2, id3] } })```

### or operators return all document which satisfy any of the condition

```User.find({ $or: [{ "name": "a" }, { "email": "z@gmail.com" }] })```

### pull operators removes a value from the array
```User.updateMany({}, { $pull: { friends: myid } })```

### push operators adds a value into the array
```User.updateMany({}, { $push: { friends: myid } })```

