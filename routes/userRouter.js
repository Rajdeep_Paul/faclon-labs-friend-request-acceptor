const express = require('express')
const router = express.Router()
const User = require('../models/user')

router.get('/user/:id', async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        res.status(200).json(user)
    } catch (error) {
        console.log(error)
    }
})
router.post('/user', async (req, res) => {
    try {
        const user = await User.create(req.body)
        res.status(200).json(user)
    } catch (error) {
        console.log(error)
    }
})
router.put('/user/:friendid', async (req, res) => {
    try {
        const { myid } = req.body
        const friendid = req.params.friendid
        const user = await User.findById(friendid)
        user.pending.push(myid)
        user.save()

        const mydetails = await User.findById(myid)
        mydetails.sentRequest.push(friendid)
        mydetails.save()

        res.status(200).json({ message: "request sent" })
    } catch (error) {
        console.log(error)
    }
})

router.put('/user/accept/:friendid', async (req, res) => {
    try {
        const { myid } = req.body
        const friendid = req.params.friendid
        const user = await User.findById(myid)
        user.friends.push(friendid)
        user.pending = user.pending.filter((id) => (id != friendid))
        user.save()

        const myFriend = await User.findById(friendid)
        myFriend.friends.push(myid)
        myFriend.save()

        res.status(200).json({ message: "Accepted" })
    } catch (error) {
        console.log(error)
    }
})

router.delete('/user/:id', async (req, res) => {
    try {
        const myid = req.params.id
        await User.findByIdAndDelete(myid)
        await User.updateMany({}, { $pull: { sentRequest: myid, friends: myid, pending: myid } })
        res.status(200).json({ message: "deleted" })
    } catch (error) {
        console.log(error)
    }
})

module.exports = router